<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1 class="mb-4">Современная цифровая конкурсная среда</h1>
                <div class="mb-4" style="font-size: 20px;">
                    Приоритетный проект в области образования. Его целью является качественное и доступное
                    проведение онлайн конкурсов для
                    граждан страны с помощью цифровых технологий.
                </div>
                <?php use IonAuth\Libraries\IonAuth;
                $ionAuth = new IonAuth();
                ?>
                <?php if (!$ionAuth->loggedIn()): ?>
                    <a href="<?php echo base_url(); ?>/auth/login" role="button" class="btn btn-red"
                       style="">Войти</a>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="advantages__card">
                    <img src="<?php echo base_url(); ?>/img/Shape.svg" alt="" style="margin-right: 15px;">
                    <div>
                        Доступ к огромной базе конкурсов и соревнований
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="advantages__card">
                    <img src="<?php echo base_url(); ?>/img/Group 7.svg" alt="" style="margin-right: 15px;">
                    <div>
                        Оценка работ лучшими экспертами страны
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="advantages__card">
                    <img src="<?php echo base_url(); ?>/img/Group 6.svg" alt="" style="margin-right: 15px;">
                    <div>
                        Реализация доступа к онлайн-конкурсам из любой точки мира
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="advantages__card">
                    <img src="<?php echo base_url(); ?>/img/Group 2.svg" alt="" style="margin-right: 15px;">
                    <div>
                        Повышение квалификации в экспертной области , проведение экспертизы и онлайн-конкурсов
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <img src="<?php echo base_url(); ?>/img/LogoRed.png" alt="" class="w-100">
            </div>
            <div class="col-md-11">
                <h5>Контакты</h5>
                <div class="contacts__content-text">
                    <p>
                        ООО «Конкурсорика»
                        648400, г. Сургут,
                        ул. Пушкина д.Колотушкина
                        Директор: Директорский Директор Директорович
                        E: fedorchuckvlad@gmail.com
                        T: +7(912)513-22-86
                        http://60571fva.student.surgu.ru/
                    </p>
                    Проведение и участие в конкурсах
                    www.Konkursorika.com
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
