<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <h1 class="text-center mb-4">Пользовательское соглашение</h1>
    <div>
        <p>Настоящее Пользовательское Соглашение (Далее Соглашение) регулирует отношения между владельцем
            60571fva.student.surgu.ru (далее Konkursorika или Администрация) с одной стороны и пользователем сайта с
            другой.
            Сайт Konkursorika не является средством массовой информации.
        </p>
        <p>Используя сайт, Вы соглашаетесь с условиями данного соглашения.
            Если Вы не согласны с условиями данного соглашения, не используйте сайт Konkursorika!
        </p>
        <p>
            Предмет соглашения
            Администрация предоставляет пользователю право на размещение на сайте следующей информации:
        </p>
        <div class="mb-3">
            Предмет соглашения
            Администрация предоставляет пользователю право на размещение на сайте следующей информации:
            <ul>
                <li>Текстовой информации</li>
                <li>Фотоматериалов</li>
            </ul>
        </div>
        <div class="mb-3">
            Права и обязанности сторон
            Пользователь имеет право:
            <ul>
                <li> осуществлять поиск информации на сайте</li>
                <li> получать информацию на сайте</li>
                <li>использовать информацию сайта в личных некоммерческих целях</li>
            </ul>
        </div>
        <div class="mb-3">
            Администрация имеет право:
            <ul>
                <li>по своему усмотрению и необходимости создавать, изменять, отменять правила</li>
                <li>ограничивать доступ к любой информации на сайте</li>
            </ul>
        </div>
        <div class="mb-3">
            Пользователь обязуется:
            <ul>
                <li>не нарушать работоспособность сайта</li>
                <li>не использовать скрипты (программы) для автоматизированного сбора информации и/или взаимодействия
                    с Сайтом и его Сервисами
                </li>
            </ul>
        </div>
        <div class="mb-3">
            Администрация обязуется:
            <ul>
                <li>поддерживать работоспособность сайта за исключением случаев, когда это невозможно по независящим
                    от
                    Администрации причинам.
                </li>
            </ul>
        </div>
        <div class="mb-3">
            Ответственность сторон
            <ul>
                <li>администрация не несет никакой ответственности за услуги, предоставляемые третьими лицами
                </li>
                <li> в случае возникновения форс-мажорной ситуации (боевые действия, чрезвычайное положение, стихийное
                    бедствие
                    и
                    т. д.) Администрация не гарантирует сохранность информации, размещённой Пользователем, а также
                    бесперебойную
                    работу информационного ресурса
                </li>
            </ul>
        </div>
        <p>
            Условия действия Соглашения
            Данное Соглашение вступает в силу при любом использовании данного сайта.
            Соглашение перестает действовать при появлении его новой версии.
            Администрация оставляет за собой право в одностороннем порядке изменять данное соглашение по своему
            усмотрению.
            Администрация не оповещает пользователей об изменении в Соглашении.
        </p>
    </div>
    <div class="text-center">
        <img class="mb-4 " src="<?php echo base_url(); ?>/img/LogoRed.png" alt="" width="65">
    </div>
</div>
<?= $this->endSection() ?>


