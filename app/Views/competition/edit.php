<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 700px;">
        <?= form_open_multipart('competition/update'); ?>
        <input type="hidden" name="id" value="<?= $competition["id"] ?>">
        <div class="form-group">
            <label for="person_id">Участник:</label>
            <select class="form-control <?= ($validation->hasError('person_id')) ? 'is-invalid' : ''; ?>"
                    name='person_id'
                    onChange="" id="person_id">
                <option value="-1">Выберите участника</option>
                <?php foreach ($person as $item): ?>
                    <option value="<?= esc($item['id']); ?>" <?php if ($competition["person_id"] == $item['id']) echo "selected"; ?>>
                        <?= esc($item['fullname']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('person_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="competition_id">Конкурс:</label>
            <select class="form-control <?= ($validation->hasError('competition_id')) ? 'is-invalid' : ''; ?>"
                    name='competition_id'
                    onChange="" id="competition_id">
                <option value="-1">Выберите конкурс</option>
                <?php foreach ($contest as $item): ?>
                    <option value="<?= esc($item['id']); ?>" <?php if ($competition["competition_id"] == $item['id']) echo "selected"; ?>>
                        <?= esc($item['name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('competition_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="competition_name">Наименование конкурсной работы:</label>
            <input type="text"
                   class="form-control <?= ($validation->hasError('competition_name')) ? 'is-invalid' : ''; ?>"
                   id="competition_name"
                   name="competition_name"
                   value="<?= $competition["competition_name"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('competition_name') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Фото</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>"
                   name="picture"
                   id="picture_url"
                   value="<?= $competition["picture_url"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>