<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($competition)) : ?>
    <div class="card mx-auto" style="max-width: 440px; font-size: 24px; padding: 30px; color: white; border: none; background: var(--red);">
        <div class="row">
            <div class="col-12">
                <div class="card-body" style="text-align:center;">
                    <?php if (is_null($competition['picture_url'])) : ?>
                        <img src="<?= base_url() ?>/img/LogoRed.png" class="w-100 mb-3" alt="">
                    <?php else:?>
                        <img src="<?= esc($competition['picture_url']); ?>" class="w-100 mb-3" alt="">
                    <?php endif ?>
                    <p class="card-title">Id участника: <?= esc($competition['person_id']); ?></p>
                    <p class="card-text">Id конкурса: <?= esc($competition['competition_id']); ?></p>
                    <p class="card-text">Наименование конкурса: <?= esc($competition['competition_name']); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php else : ?>
    <p>Невозможно найти работу.</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>