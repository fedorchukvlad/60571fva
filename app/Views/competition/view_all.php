<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <h2 class="pt-5 mb-5 text-center">Конкурсные работы</h2>
    <div class="row mb-3" style="text-align: center; align-items:center; padding-bottom: 15px; border-bottom: 2px solid grey">
        <div class="col-1">Фото</div>
        <div class="col-2">Id участника</div>
        <div class="col-2">Id конкурса</div>
        <div class="col-3">Наименование конкурса</div>
        <div class="col-4"></div>
    </div>
    <?php if (!empty($competition) && is_array($competition)) : ?>
        <?php foreach ($competition as $item): ?>
            <div class="row mb-3 py-3"
                 style="color: black; background: white; align-items: center; border-radius: 5px; text-align:center;">
                <div class="col-1">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <img src="<?= base_url() ?>/img/LogoRed.png" class="w-100" alt="">
                    <?php else:?>
                        <img src="<?= esc($item['picture_url']); ?>" class="w-100" alt="">
                    <?php endif ?>
                </div>
                <div class="col-2"><?= esc($item['person_id']); ?></div>
                <div class="col-2"><?= esc($item['competition_id']); ?></div>
                <div class="col-3">
                    <?= esc($item['competition_name']); ?>
                </div>
                <div class="col-4 text-right">
                    <a href="<?= base_url() ?>/competition/view/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="bi:card-text" data-inline="false"></span>
                    </a>
                    <a href="<?= base_url() ?>/competition/edit/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                    </a>
                    <a href="<?= base_url() ?>/competition/delete/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="fluent:delete-16-regular" data-inline="false"></span>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Невозможно найти работу.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
