<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('competition/store'); ?>
        <div class="form-group">
            <label for="person_id">Участник:</label>
            <select class="form-control <?= ($validation->hasError('person_id')) ? 'is-invalid' : ''; ?>"
                    name='person_id'
                    onChange="" id="person_id">
                <option value="-1">Выберите участника</option>
                <?php foreach ($person as $item): ?>
                    <option value="<?= esc($item['id']); ?>">
                        <?= esc($item['fullname']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('person_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="competition_id">Конкурс:</label>
            <select class="form-control <?= ($validation->hasError('competition_id')) ? 'is-invalid' : ''; ?>"
                    name='competition_id'
                    onChange="" id="competition_id">
                <option value="-1">Выберите конкурс</option>
                <?php foreach ($contest as $item): ?>
                    <option value="<?= esc($item['id']); ?>">
                        <?= esc($item['name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('competition_id') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="competition_name">Наименование конкурсной работы:</label>
            <input type="text"
                   class="form-control <?= ($validation->hasError('competition_name')) ? 'is-invalid' : ''; ?>"
                   id="competition_name"
                   name="competition_name"
                   value="<?= old('competition_name'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('competition_name') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="picture_url">Фото</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
                   name="picture" id="picture_url">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>