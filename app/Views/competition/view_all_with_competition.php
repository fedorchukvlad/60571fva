<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container">
    <h2 class="pt-5 mb-5 text-center">Конкурсные работы</h2>
    <div class="d-flex justify-content-between mb-2">
        <?= $pager->links('group1', 'my_page') ?>
        <?= form_open('competition/viewAllWithCompetition', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-danger" type="submit">На странице</button>
        </form>
        <?= form_open('competition/viewAllWithCompetition', ['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="ФИО или наименование конкурса"
               aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-danger" type="submit">Найти</button>
        </form>
    </div>
    <div class="row mb-3"
         style="text-align: center; align-items:center; padding-bottom: 15px; border-bottom: 2px solid grey">
        <div class="col-3">Наименование работы</div>
        <div class="col-2">ФИО</div>
        <div class="col-2">Конкурс</div>
        <div class="col-1">Фото</div>
        <div class="col-4"></div>
    </div>
    <?php if (!empty($competition) && is_array($competition)) : ?>
        <?php foreach ($competition as $item): ?>
            <div class="row mb-3 py-3"
                 style="color: black; background: white; align-items: center; border-radius: 5px; text-align:center;">
                <div class="col-3">
                    <?= esc($item['competition_name']); ?>
                </div>
                <div class="col-2"><?= esc($item['fullname']); ?></div>
                <div class="col-2"><?= esc($item['name']); ?></div>
                <div class="col-1">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <img src="<?= base_url() ?>/img/LogoRed.png" class="w-100" alt="">
                    <?php else:?>
                        <img src="<?= esc($item['picture_url']); ?>" class="w-100" alt="">
                    <?php endif ?>
                </div>

                <div class="col-4 text-right">
                    <a href="<?= base_url() ?>/competition/view/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="bi:card-text" data-inline="false"></span>
                    </a>
                    <a href="<?= base_url() ?>/competition/edit/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span>
                    </a>
                    <a href="<?= base_url() ?>/competition/delete/<?= esc($item['id']); ?>"
                       class="btn btn-red">
                        <span class="iconify" data-icon="fluent:delete-16-regular" data-inline="false"></span>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Невозможно найти работу.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

