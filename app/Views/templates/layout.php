<!DOCTYPE html>
<head>
    <title>Konkursorika</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        :root {
            --red: #f55159;
            --opacity-red: #f5515970;
        }

        a:hover {
            text-decoration: none;
        }

        h1 {
            font-size: 54px;
        }

        .btn-red {
            font-size: 24px;
            padding: 5px 35px;
            border-radius: 5px;
            background: var(--red);
            color: white;
        }

        .btn-red:hover {
            background: transparent;
            border: 1px solid var(--red);
            color: var(--red);
        }

        .header {
            padding: 115px 0;
            background: url('/img/IdetVGoru.png') right bottom no-repeat;
        }

        .advantages {
            padding: 50px 0 20px;
            background: url('/img/pattern-red.png') center;
        }

        .advantages__card {
            min-height: 130px;
            margin-bottom: 30px;
            padding: 20px;
            border-radius: 5px;
            background: white;
            display: flex;
            align-items: center;
            justify-content: flex-start;
            font-size: 20px;
        }

        .contacts {
            padding: 50px 0;
            font-size: 14px;
        }

        .page-item .page-link {
            border-color: var(--opacity-red);
            color:var(--red);
        }

        .page-item.active .page-link {
            background: var(--red);
            border-color: var(--red);
        }
    </style>
</head>
<body style="font-family: 'Montserrat', sans-serif; font-size: 18px">
<div class="container">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background: var(--red);">
        <a class="navbar-brand" style="" href="<?= base_url() ?>">
            <img src="<?= base_url() ?>/img/Logo.png" alt="" style="width: 65px;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Конкурсные работы
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?= base_url() ?>/competition">Все конкурсные работы</a>
                        <a class="dropdown-item" href="<?= base_url() ?>/competition/viewAllWithCompetition">Все
                            участники</a>
                        <a class="dropdown-item" href="<?= base_url()?>/competition/store">Добавить конкурсную работу</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Мой профиль</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">Мой профиль</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <?php use IonAuth\Libraries\IonAuth;

                $ionAuth = new IonAuth();
                ?>
                <?php if (!$ionAuth->loggedIn()): ?>
                    <div class="nav-item dropdown">
                        <a class="nav-link active" href="<?= base_url() ?>/auth/login">
                            <span class="fas fa fa-sign-in-alt" style="color:white; margin-right: 10px;"></span>
                            Вход
                        </a>
                    </div>
                <?php else: ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="true">
                            <span class="fas fa fa-user-alt" style="color:white"></span>
                            <?php echo $ionAuth->user()->row()->email; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="<?= base_url() ?>/auth/logout">
                                <span class="fas fa fa-sign-in-alt" style="color:white"></span>
                                Выход
                            </a>
                        </div>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </nav>
</div>
<main role="main" style="margin-top: 110px;">


    <?= $this->renderSection('content') ?>
</main>
<footer class="text-center py-3">
    <div class="container">
        © Федорчук Владислав 2021 | <a href="<?php echo base_url(); ?>/index.php/pages/view/agreement"
                                       style="color: black;">Пользовательское соглашение</a>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
</body>
</html>
