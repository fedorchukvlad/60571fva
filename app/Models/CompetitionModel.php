<?php


namespace App\Models;


use CodeIgniter\Model;

class CompetitionModel extends Model
{
    protected $table = 'player'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['person_id', 'competition_id', 'competition_name', 'picture_url'];

    public function getPlayer($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getPlayerWithCompetition($id = null, $search = '')
    {
        $builder = $this->select('*, player.id')
            ->join('person', 'player.person_id = person.id', 'LEFT')
            ->join('competition', 'player.competition_id = competition.id', 'LEFT')
            ->like('fullname', $search, 'both', null, true)
            ->orlike('name', $search, 'both', null, true);
        if (!is_null($id)) {
            return $builder->where(['player.id' => $id])->first();
        }
        return $builder;
    }
}