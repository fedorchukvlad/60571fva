<?php


namespace App\Models;


use CodeIgniter\Model;

class ContestModel extends Model
{
    protected $table = 'competition'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['name'];

    public function getContest($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}