<?php


namespace App\Models;


use CodeIgniter\Model;

class PersonModel extends Model
{
    protected $table = 'person'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['fullname'];

    public function getPerson($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}