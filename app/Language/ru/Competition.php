<?php
return [
    'admin_permission_needed' => 'Доступ запрещён, нужны права администратора!',
    'competition_create_success' => 'Запись успешно создана',
    'login_with_google'=>'Войти с помощью Google'
];