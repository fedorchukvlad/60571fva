<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class DbSeeder extends Seeder
{
	public function run()
	{
	    /*table person*/
        $data = [
            'fullname' => 'Лапшина Анна Михайловна',
            'birthday' => '2015-06-12',
        ];
        $this->db->table('person')->insert($data);
        $data = [
            'fullname' => 'Елизаров Андрей Владимирович',
            'birthday' => '1993-08-06',
        ];
        $this->db->table('person')->insert($data);
        $data = [
            'fullname' => 'Зыкова Мария Романовна',
            'birthday' => '1992-06-15',
        ];
        $this->db->table('person')->insert($data);
        /*end table person*/
        /*table competition*/
        $data = [
            'name' => 'Конкурс детских рисунков',
            'date' => '2021-05-14',
        ];
        $this->db->table('competition')->insert($data);
        $data = [
            'name' => 'Конкурс подделок к 9 мая',
            'date' => '2021-05-27',
        ];
        $this->db->table('competition')->insert($data);
        $data = [
            'name' => 'Мировой турнир искусств (скульптура)',
            'date' => '2021-06-03',
        ];
        $this->db->table('competition')->insert($data);
        /*end table competition*/
        /*table player*/
        $data = [
            'person_id'=>'1',
            'competition_id'=>'1',
            'competition_name' => 'Домик у горы',
        ];
        $this->db->table('player')->insert($data);
        $data = [
            'person_id'=>'2',
            'competition_id'=>'2',
            'competition_name' => 'Дом у озера',
        ];
        $this->db->table('player')->insert($data);
        $data = [
            'person_id'=>'3',
            'competition_id'=>'3',
            'competition_name' => 'Домик в деревне',
        ];
        $this->db->table('player')->insert($data);
        /*end table player*/
	}
}
