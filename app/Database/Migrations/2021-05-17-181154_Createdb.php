<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createdb extends Migration
{
	public function up()
	{
        // Drop table 'person' if it exists
        $this->forge->dropTable('person', true);
        // Table structure for table 'person'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'fullname' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
            'birthday' => [
                'type'       => 'DATE',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('person');

        // Drop table 'competition' if it exists
        $this->forge->dropTable('competition', true);
        // Table structure for table 'competition'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
            'date' =>[
                'type'       => 'DATE',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('competition');

//        // Drop table 'jury' if it exists
//        $this->forge->dropTable('jury', true);
//        // Table structure for table 'jury'
//        $this->forge->addField([
//            'id' => [
//                'type'           => 'INT',
//                'unsigned'       => true,
//                'auto_increment' => true,
//            ],
//            'person_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//            'competition_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//        ]);
//        $this->forge->addKey('id', true);
//        $this->forge->addForeignKey('person_id', 'person', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->addForeignKey('competition_id', 'competition', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->createTable('jury');

        // Drop table 'player' if it exists
        $this->forge->dropTable('player', true);
        // Table structure for table 'player'
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'person_id' => [
                'type'       => 'INT',
                'unsigned'   => true,
                'null'       => false,
            ],
            'competition_id' => [
                'type'       => 'INT',
                'unsigned'   => true,
                'null'       => false,
            ],
            'competition_name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('person_id', 'person', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->addForeignKey('competition_id', 'competition', 'id', 'RESTRICT', 'RESTRICT');
        $this->forge->createTable('player');

//        // Drop table 'criterion' if it exists
//        $this->forge->dropTable('criterion', true);
//        // Table structure for table 'criterion'
//        $this->forge->addField([
//            'id' => [
//                'type'           => 'INT',
//                'unsigned'       => true,
//                'auto_increment' => true,
//            ],
//            'competition_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//            'criterion_name' => [
//                'type'       => 'VARCHAR',
//                'constraint' => '255',
//                'null'       => false,
//            ],
//            'max_point' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'default'    => 0,
//            ],
//        ]);
//        $this->forge->addKey('id', true);
//        $this->forge->addForeignKey('competition_id', 'competition', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->createTable('criterion');
//
//        // Drop table 'score' if it exists
//        $this->forge->dropTable('score', true);
//        // Table structure for table 'score'
//        $this->forge->addField([
//            'id' => [
//                'type'           => 'INT',
//                'unsigned'       => true,
//                'auto_increment' => true,
//            ],
//            'person_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//            'criterion_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//            'jury_id' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'null'       => false,
//            ],
//            'score' => [
//                'type'       => 'INT',
//                'unsigned'   => true,
//                'default'    => 0,
//            ],
//        ]);
//        $this->forge->addKey('id', true);
//        $this->forge->addForeignKey('person_id', 'person', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->addForeignKey('criterion_id', 'criterion', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->addForeignKey('jury_id', 'jury', 'id', 'RESTRICT', 'RESTRICT');
//        $this->forge->createTable('score');

    }

	public function down()
	{
        $this->forge->dropTable('person', true);
        $this->forge->dropTable('competition', true);
//        $this->forge->dropTable('jury', true);
        $this->forge->dropTable('player', true);
//        $this->forge->dropTable('criterion', true);
//        $this->forge->dropTable('score', true);
	}
}
