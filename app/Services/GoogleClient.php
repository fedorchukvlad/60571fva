<?php


namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('43450533282-qk8q5okir5m6nsk8rvpbs6srlvqhlm31.apps.googleusercontent.com');
        $this->google_client->setClientSecret('8PcfKWKmz3zb1cuAxIkxm7ik');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }
}