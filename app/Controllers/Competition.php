<?php


namespace App\Controllers;

use App\Models\CompetitionModel;
use App\Models\ContestModel;
use App\Models\PersonModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Competition extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CompetitionModel();
        $data ['competition'] = $model->getPlayer();
        echo view('competition/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CompetitionModel();
        $data ['competition'] = $model->getPlayer($id);
        echo view('competition/view', $this->withIon($data));
    }

    public function viewAllWithCompetition()
    {
        if ($this->ionAuth->isAdmin()) {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form', 'url']);
            $model = new CompetitionModel();
            $data['competition'] = $model->getPlayerWithCompetition(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('competition/view_all_with_competition', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('Competition.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }


    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $model = new ContestModel();
        $data['contest'] = $model->getContest();
        $model = new PersonModel();
        $data['person'] = $model->getPerson();
        echo view('competition/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'person_id' => 'required|greater_than[-1]',
                'competition_id' => 'required|greater_than[-1]',
                'competition_name' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]'
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new CompetitionModel();
            $data = [
                'person_id' => $this->request->getPost('person_id'),
                'competition_id' => $this->request->getPost('competition_id'),
                'competition_name' => $this->request->getPost('competition_name'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Competition.competition_create_success'));
            return redirect()->to('/competition');
        } else {
            return redirect()->to('/competition/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CompetitionModel();

        helper(['form']);
        $data ['competition'] = $model->getPlayer($id);
        $data ['validation'] = \Config\Services::validation();
        $model = new ContestModel();
        $data['contest'] = $model->getContest();
        $model = new PersonModel();
        $data['person'] = $model->getPerson();
        echo view('competition/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/competition/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'person_id' => 'required|greater_than[-1]',
                'competition_id' => 'required|greater_than[-1]',
                'competition_name' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]'
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new CompetitionModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'person_id' => $this->request->getPost('person_id'),
                'competition_id' => $this->request->getPost('competition_id'),
                'competition_name' => $this->request->getPost('competition_name'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/competition');
        } else {
            return redirect()->to('/competition/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new CompetitionModel();
        $model->delete($id);
        return redirect()->to('/competition');
    }
}