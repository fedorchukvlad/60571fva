-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 16 2021 г., 18:11
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571fva`
--

-- --------------------------------------------------------

--
-- Структура таблицы `competition`
--

CREATE TABLE `competition` (
  `id` int UNSIGNED NOT NULL COMMENT 'Id конкурса',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Название конкурса',
  `date` date NOT NULL COMMENT 'Дата проведения конкурса'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `competition`
--

INSERT INTO `competition` (`id`, `name`, `date`) VALUES
(1, 'Конкурс детских рисунков', '2021-05-14'),
(2, 'Конкурс подделок к 9 мая', '2021-05-27'),
(3, 'Мировой турнир искусств (скульптура)', '2021-06-03');

-- --------------------------------------------------------

--
-- Структура таблицы `criterion`
--

CREATE TABLE `criterion` (
  `id` int UNSIGNED NOT NULL COMMENT 'ID критерия',
  `competition_id` int UNSIGNED NOT NULL COMMENT 'Id конкурса',
  `criterion_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Название критерия',
  `max_point` int UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во очков'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `criterion`
--

INSERT INTO `criterion` (`id`, `competition_id`, `criterion_name`, `max_point`) VALUES
(1, 1, 'Красочность', 10),
(3, 1, 'Актуальность работы', 10),
(4, 1, 'Творческая индивидуальность', 20),
(5, 2, 'Соответствие содержания творческой работы заявленной тематике', 10),
(6, 2, 'Полнота и образность раскрытия темы', 10),
(7, 2, 'Оригинальность идеи, новаторство, творческий подход', 15),
(9, 3, 'Актуальность конкурсной работы', 10),
(10, 3, 'Творческая индивидуальность', 10),
(11, 3, 'Выразительность применяемых методов', 30);

-- --------------------------------------------------------

--
-- Структура таблицы `jury`
--

CREATE TABLE `jury` (
  `id` int UNSIGNED NOT NULL COMMENT 'Id члена жури',
  `person_id` int UNSIGNED NOT NULL COMMENT 'Id персоны',
  `competition_id` int UNSIGNED NOT NULL COMMENT 'Id конкурса'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `jury`
--

INSERT INTO `jury` (`id`, `person_id`, `competition_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2),
(4, 4, 2),
(5, 5, 2),
(6, 6, 3),
(7, 7, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `person`
--

CREATE TABLE `person` (
  `id` int UNSIGNED NOT NULL COMMENT 'Id персоны',
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ФИО Персоны',
  `birthday` date NOT NULL COMMENT 'Дата рождения персоны'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `person`
--

INSERT INTO `person` (`id`, `fullname`, `birthday`) VALUES
(1, 'Лапшина Анна Михайловна ', '2000-02-02'),
(2, 'Устинова Арина Платоновна', '1993-04-08'),
(3, 'Елизаров Андрей Владимирович', '1993-08-06'),
(4, 'Зыкова Мария Романовна', '1992-06-15'),
(5, 'Нечаева София Александровна', '1987-07-10'),
(6, 'Журавлева Валерия Максимовна', '1993-11-12'),
(7, 'Шевелева Маргарита Владимировна', '1983-04-21'),
(8, 'Орехов Роман Даниилович', '2013-08-19'),
(9, 'Николаева Вера Арсентьевна', '2013-05-15'),
(10, 'Калинин Николай Максимович', '2013-10-04'),
(11, 'Воронин Роман Матвеевич', '2014-04-17'),
(12, 'Кожевников Константин Дмитриевич', '2012-08-07'),
(13, 'Васильев Максим Владимирович', '2014-01-10'),
(14, 'Осипова Александра Васильевна', '2013-08-29'),
(15, 'Комаров Григорий Владимирович', '2014-02-01'),
(16, 'Царев Андрей Сергеевич', '2013-10-03'),
(17, 'Андреева Софья Макаровна', '2013-04-03'),
(18, 'Максимов Роман Матвеевич', '2014-01-11'),
(19, 'Грачев Георгий Ильич', '2013-02-20'),
(20, 'Дьякова Арина Артуровна', '2013-12-06'),
(21, 'Орлова Елизавета Егоровна', '2000-08-19'),
(22, 'Попова Александра Гордеевна', '2000-01-05'),
(23, 'Кузина Серафима Егоровна', '2000-11-17'),
(24, 'Ушаков Павел Львович', '2000-12-26'),
(25, 'Русакова Александра Егоровна', '2000-10-27'),
(26, 'Кузнецов Даниил Алексеевич', '1999-09-14'),
(27, 'Громова Алёна Ярославовна', '2000-12-14'),
(28, 'Титова Валерия Дмитриевна', '2000-11-24'),
(29, 'Фокин Максим Владиславович', '2000-07-11'),
(30, 'Тихомиров Гордей Павлович', '2000-10-19');

-- --------------------------------------------------------

--
-- Структура таблицы `player`
--

CREATE TABLE `player` (
  `id` int UNSIGNED NOT NULL COMMENT 'Id участника',
  `person_id` int UNSIGNED NOT NULL COMMENT 'Id персоны',
  `competition_id` int UNSIGNED NOT NULL COMMENT 'Id конкурса',
  `competition_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Наименование конкурсной работы'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `player`
--

INSERT INTO `player` (`id`, `person_id`, `competition_id`, `competition_name`) VALUES
(1, 8, 1, 'Домик у горы'),
(2, 9, 1, 'Дом у озера'),
(3, 10, 1, 'Домик в деревне'),
(4, 11, 1, 'Домик на курьих ножках'),
(5, 12, 1, 'Дом на плоту'),
(6, 13, 2, 'Голубь мира из салфеток'),
(7, 14, 2, 'Открытка с гвоздиками'),
(8, 15, 2, 'Классная звезда в стиле оригами'),
(9, 16, 2, 'Панорама битвы'),
(10, 17, 2, 'Обелиск в память героям'),
(11, 18, 2, 'Танк из полимерной глины'),
(12, 19, 2, 'Вертолёт из полимерной глины'),
(13, 20, 2, 'Танк из фетра'),
(14, 21, 3, 'Скульптура \"Памятник погибшим байкерам\" Белая церковь, Украина'),
(15, 22, 3, 'Скульптура \"Галушки\" Полтава, Украина'),
(16, 23, 3, 'Скульптура \"Пасхальное солнце\" Ивано-Франковск, Украина'),
(17, 24, 3, 'Скульптура \"Пузатая фигура любителя вина\" Львов, Украина'),
(18, 25, 3, 'Скульптура \"Романтический почтальон возле почтамта.\"\r\nНижний Новгород, Россия'),
(19, 26, 3, 'Скульптура орла - символа Кавказских Минеральных Вод\r\nПятигорск, Россия'),
(20, 27, 3, 'Скульптура \"Подольск город рабочего класса\"\r\nПодольск, Россия'),
(21, 28, 3, 'Скульптура студента-политехника\r\nСанкт-Петербург, Россия'),
(22, 29, 3, 'Скульптура \"Молодая модница\"\r\nНижний Новгород, Россия'),
(23, 30, 3, 'Цветочная скульптура \"Петух\"\r\nКрасноярск, Россия');

-- --------------------------------------------------------

--
-- Структура таблицы `score`
--

CREATE TABLE `score` (
  `id` int UNSIGNED NOT NULL COMMENT 'ID оценки',
  `person_id` int UNSIGNED NOT NULL COMMENT 'Id участника',
  `criterion_id` int UNSIGNED NOT NULL COMMENT 'ID критерия',
  `jury_id` int UNSIGNED NOT NULL COMMENT 'Id члена жури',
  `score` int UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Поставленный бал'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `score`
--

INSERT INTO `score` (`id`, `person_id`, `criterion_id`, `jury_id`, `score`) VALUES
(1, 1, 1, 1, 5),
(2, 2, 1, 1, 6),
(3, 3, 1, 1, 7),
(4, 4, 1, 1, 8),
(5, 5, 1, 1, 9),
(6, 1, 1, 2, 9),
(7, 2, 1, 2, 9),
(8, 3, 1, 2, 5),
(9, 4, 1, 2, 2),
(10, 5, 1, 2, 10),
(11, 1, 3, 1, 2),
(12, 2, 3, 1, 5),
(13, 3, 3, 1, 6),
(14, 4, 3, 1, 7),
(15, 5, 3, 1, 5),
(16, 1, 3, 2, 8),
(17, 2, 3, 2, 4),
(18, 3, 3, 2, 9),
(19, 4, 3, 2, 2),
(20, 5, 3, 2, 6),
(21, 1, 4, 1, 12),
(22, 2, 4, 1, 18),
(23, 3, 4, 1, 14),
(24, 4, 4, 1, 4),
(25, 5, 4, 1, 15),
(26, 1, 4, 2, 9),
(27, 2, 4, 2, 13),
(28, 3, 4, 2, 14),
(29, 4, 4, 2, 18),
(30, 5, 4, 2, 19);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `competition`
--
ALTER TABLE `competition`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `criterion`
--
ALTER TABLE `criterion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `criterion_competition_id_foreign` (`competition_id`);

--
-- Индексы таблицы `jury`
--
ALTER TABLE `jury`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jury_person_id_foreign` (`person_id`),
  ADD KEY `jury_competition_id_foreign` (`competition_id`);

--
-- Индексы таблицы `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `player_person_id_foreign` (`person_id`),
  ADD KEY `player_competition_id_foreign` (`competition_id`);

--
-- Индексы таблицы `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id`),
  ADD KEY `score_person_id_foreign` (`person_id`),
  ADD KEY `score_criterion_id_foreign` (`criterion_id`),
  ADD KEY `score_jury_id_foreign` (`jury_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `competition`
--
ALTER TABLE `competition`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id конкурса', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `criterion`
--
ALTER TABLE `criterion`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID критерия', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `jury`
--
ALTER TABLE `jury`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id члена жури', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `person`
--
ALTER TABLE `person`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id персоны', AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `player`
--
ALTER TABLE `player`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id участника', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `score`
--
ALTER TABLE `score`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID оценки', AUTO_INCREMENT=31;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `criterion`
--
ALTER TABLE `criterion`
  ADD CONSTRAINT `criterion_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `jury`
--
ALTER TABLE `jury`
  ADD CONSTRAINT `jury_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `jury_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `player_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `player_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `score_criterion_id_foreign` FOREIGN KEY (`criterion_id`) REFERENCES `criterion` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `score_jury_id_foreign` FOREIGN KEY (`jury_id`) REFERENCES `jury` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `score_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
